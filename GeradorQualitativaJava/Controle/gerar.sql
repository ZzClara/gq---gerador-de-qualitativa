CREATE TABLE arquivo (
    id integer NOT NULL,
    nome character varying,
    arquivo bytea,
    CONSTRAINT pk_arquivo PRIMARY KEY (id)
);
CREATE SEQUENCE seq_arquivo INCREMENT 1 START 1;