public boolean insertFile( File f ){
    Connection c = this.getConnection();
    try {
        PreparedStatement ps = c.prepareStatement("INSERT INTO arquivo( id, nome, arquivo ) VALUES ( nextval('seq_arquivo'), ?, ? )");
 
        InputStream is = new FileInputStream( f );
        byte[] bytes = new byte[(int)f.length() ];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
 
        ps.setString( 1, f.getName() );
        ps.setBytes( 2, bytes );
        ps.execute();
        ps.close();
        c.close();
        return true;
 
    } catch (SQLException ex) {
        ex.printStackTrace();
    } catch (IOException ex) {
        ex.printStackTrace();
    }
    return false;