# Aplicação Avaliativa

Esta aplicação será desenvolvida para facilitar o trabalho do professor, que visa contribuir para a geração da nota qualitativa do aluno de acordo com os critérios necessários para formular a mesma. A aplicação será dividida em cinco critérios, onde o objetivo central será o acompanhamento do aluno e sua avaliação em cada quesito. Ao final do preenchimento de cada quesito, a nota qualitativa será gerada.

# Funcionalidades

    * Facilitar para o professor o calculo da nota;
    * Ter uma precisão dos pontos em que o aluno precisa melhorar;
    * Ter um acompanhamento de forma simples do desempenho;
    * Uma média rápida e pratica.
    