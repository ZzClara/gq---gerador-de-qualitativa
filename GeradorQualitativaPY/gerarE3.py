"""
Feito em 2019
@descripition: classe controle de gerar
@Author:Emily,Clara,Bia
"""
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.enfer3_ui import Ui_enfer3
import sys

app = QtWidgets.QApplication(sys.argv)
enfer3 = QtWidgets.QWidget()
ui = Ui_enfer3()
ui.setupUi(enfer3)
enfer3.show()
sys.exit(app.exec_())