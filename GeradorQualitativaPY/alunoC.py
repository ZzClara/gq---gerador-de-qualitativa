"""
Feito em 2019
@description: Tela do executavel da tela instruções
@author:Emily,Clara,Bia
"""
#Importaçao e executavel da tela Instruções
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtWidgets
from Visual.alunoC_ui import Ui_Form
import sys

app = QtWidgets.QApplication(sys.argv)
Form = QtWidgets.QWidget()
ui = Ui_Form()
ui.setupUi(Form)
Form.show()
sys.exit(app.exec_())