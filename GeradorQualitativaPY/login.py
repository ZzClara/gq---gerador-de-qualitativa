"""
@descripition: Executavel da tela Login
@author:Emily,Clara,Bia
"""

#Tela de execuçao da tela Login
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.Login_ui import Ui_janelalogin
import sys

app = QApplication(sys.argv)
janelalogin = QMainWindow()
ui = Ui_janelalogin()
ui.setupUi(janelalogin)
janelalogin.show()
sys.exit(app.exec_())