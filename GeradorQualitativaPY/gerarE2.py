"""
Feito em 2019
@descripition: classe controle de gerar
@Author:Emily,Clara,Bia
"""
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.enfer2_ui import Ui_enfer2
import sys

app = QtWidgets.QApplication(sys.argv)
enfer2 = QtWidgets.QWidget()
ui = Ui_enfer2()
ui.setupUi(enfer2)
enfer2.show()
sys.exit(app.exec_())