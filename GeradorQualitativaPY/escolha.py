"""
Feito em 2019
@description: Tela do executavel da tela instruções
@author:Emily,Clara,Bia
"""
#Importaçao e executavel da tela Instruções
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.escolha_ui import Ui_Form
import sys
app = QApplication(sys.argv)
esc = QMainWindow()
escolha = Ui_Form()
escolha.setupUi(esc)
esc.show()
sys.exit(app.exec_())    