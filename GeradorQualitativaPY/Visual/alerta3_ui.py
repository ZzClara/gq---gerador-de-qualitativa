# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'alerta3.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
"""
Feito em 2019
@descripition: classe controle de Alerta
@Author:Emily,Clara,Bia
"""

from PyQt5 import QtCore, QtGui, QtWidgets
#Criação da classe visual alerta
class Ui_alerta3(object):
    def setupUi(self, alerta3):
        alerta3.setObjectName("alerta3")
        alerta3.resize(295, 180)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(243, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(255, 255, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(243, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(120, 120, 120))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(243, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(243, 0, 0))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        alerta3.setPalette(palette)
        self.centralwidget = QtWidgets.QWidget(alerta3)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 10, 161, 31))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(25)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 40, 291, 111))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(22)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        #Criação do botão
        self.fechar = QtWidgets.QPushButton(self.centralwidget)
        self.fechar.setGeometry(QtCore.QRect(80, 150, 158, 23))
        self.fechar.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.fechar.setObjectName("fechar")
        alerta3.setCentralWidget(self.centralwidget)
        #Slots de click atribuidos ao botao
        self.retranslateUi(alerta3)
        self.fechar.clicked.connect(alerta3.close)
        QtCore.QMetaObject.connectSlotsByName(alerta3)
        #metodo de atribuiição
    def retranslateUi(self, alerta3):
        _translate = QtCore.QCoreApplication.translate
        alerta3.setWindowTitle(_translate("alerta3", "MainWindow"))
        self.label.setText(_translate("alerta3", "! ALERTA !"))
        self.label_2.setText(_translate("alerta3", "<html><head/><body><p><span style=\" font-size:16pt;\">CAMPO EM BRANCO.</span></p><p><span style=\" font-size:16pt;\">PREENCHA CORRETAMENTE </span></p><p><span style=\" font-size:16pt;\">TODOS OS CAMPOIS!</span></p></body></html>"))
        self.fechar.setText(_translate("alerta3", "X Fechar"))
    