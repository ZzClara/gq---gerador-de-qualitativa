# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'select.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
"""
Feito em 2019
@descripition:Selecionar
@Author:Emily,Clara,Bia
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from Visual.infor1_ui import Ui_Form
from Visual.infor2_ui import Ui_infor2
from Visual.infor3_ui import Ui_infor3
from Visual.guia1_ui import Ui_guia1
from Visual.guia2_ui import Ui_guia2
from Visual.evt3_ui import Ui_evt3
from Visual.enfer1_ui import Ui_enfer1
from Visual.enfer2_ui import Ui_enfer2
from Visual.enfer3_ui import Ui_enfer3

class Ui_select(object):
    def setupUi(self, select):
        select.setObjectName("select")
        select.resize(579, 473)
        self.label = QtWidgets.QLabel(select)
        self.label.setGeometry(QtCore.QRect(0, 0, 581, 211))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("fi.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.gridLayoutWidget = QtWidgets.QWidget(select)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 220, 561, 231))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.guia2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.guia2.setFont(font)
        self.guia2.setObjectName("guia2")
        self.gridLayout.addWidget(self.guia2, 4, 1, 1, 1)
        self.infor2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.infor2.setFont(font)
        self.infor2.setObjectName("infor2")
        self.gridLayout.addWidget(self.infor2, 4, 2, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 5, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.guia1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.guia1.setFont(font)
        self.guia1.setObjectName("guia1")
        self.gridLayout.addWidget(self.guia1, 2, 1, 1, 1)
        self.infor1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.infor1.setFont(font)
        self.infor1.setObjectName("infor1")
        self.gridLayout.addWidget(self.infor1, 2, 2, 1, 1)
        self.enfer1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.enfer1.setFont(font)
        self.enfer1.setObjectName("enfer1")
        self.gridLayout.addWidget(self.enfer1, 2, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.enfer2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.enfer2.setFont(font)
        self.enfer2.setObjectName("enfer2")
        self.gridLayout.addWidget(self.enfer2, 4, 0, 1, 1)
        self.enfer3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.enfer3.setFont(font)
        self.enfer3.setObjectName("enfer3")
        self.gridLayout.addWidget(self.enfer3, 6, 0, 1, 1)
        self.evt3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.evt3.setFont(font)
        self.evt3.setObjectName("evt3")
        self.gridLayout.addWidget(self.evt3, 6, 1, 1, 1)
        self.infor3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.infor3.setFont(font)
        self.infor3.setObjectName("infor3")
        self.gridLayout.addWidget(self.infor3, 6, 2, 1, 1)
        self.select = select


        self.retranslateUi(select)
        self.enfer1.clicked.connect(self.botaoEnfer1)
        self.enfer2.clicked.connect(self.botaoEnfer2)
        self.enfer3.clicked.connect(self.botaoEnfer3)
        self.infor1.clicked.connect(self.botaoInfor1)
        self.infor2.clicked.connect(self.botaoInfor2)
        self.infor3.clicked.connect(self.botaoInfor3)
        self.guia1.clicked.connect(self.botaoGuia1)
        self.guia2.clicked.connect(self.botaoGuia2)
        self.evt3.clicked.connect(self.botaoEvt3)
        QtCore.QMetaObject.connectSlotsByName(select)



    def botaoEnfer1(self):
        self.enfer1 = QtWidgets.QMainWindow()
        self.ui = Ui_enfer1()
        self.ui.setupUi(self.enfer1)
        self.enfer1.show()
        self.select.close()

    def botaoEnfer2(self):
        self.enfer2 = QtWidgets.QMainWindow()
        self.ui = Ui_enfer2()
        self.ui.setupUi(self.enfer2)
        self.enfer2.show()
        self.select.close()

    def botaoEnfer3(self):
        self.enfer3 = QtWidgets.QMainWindow()
        self.ui = Ui_enfer3()
        self.ui.setupUi(self.enfer3)
        self.enfer3.show()
        self.select.close()

    def botaoInfor1(self):
        self.infor1 = QtWidgets.QMainWindow()
        self.ui = Ui_Form()
        self.ui.setupUi(self.infor1)
        self.infor1.show()
        self.select.close()

    def botaoInfor2(self):
        self.infor2 = QtWidgets.QMainWindow()
        self.ui = Ui_infor2()
        self.ui.setupUi(self.infor2)
        self.infor2.show()
        self.select.close()

    def botaoInfor3(self):
        self.infor3 = QtWidgets.QMainWindow()
        self.ui = Ui_infor3()
        self.ui.setupUi(self.infor3)
        self.infor3.show()
        self.select.close()

    def botaoGuia1(self):
        self.guia1 = QtWidgets.QMainWindow()
        self.ui = Ui_guia1()
        self.ui.setupUi(self.guia1)
        self.guia1.show()
        self.select.close()

    def botaoGuia2(self):
        self.guia2 = QtWidgets.QMainWindow()
        self.ui = Ui_guia2()
        self.ui.setupUi(self.guia2)
        self.guia2.show()
        self.select.close()

    def botaoEvt3(self):
        self.evt3 = QtWidgets.QMainWindow()
        self.ui = Ui_evt3()
        self.ui.setupUi(self.evt3)
        self.evt3.show()
        self.select.close()



    def retranslateUi(self, select):
        _translate = QtCore.QCoreApplication.translate
        select.setWindowTitle(_translate("select", "Form"))
        self.guia2.setText(_translate("select", "Guia de Turismo"))
        self.infor2.setText(_translate("select", "Informática"))
        self.label_5.setText(_translate("select", "3º ano"))
        self.label_2.setText(_translate("select", "Selecione a Turma:"))
        self.label_3.setText(_translate("select", "1º ano"))
        self.guia1.setText(_translate("select", "Guia de Turismo"))
        self.infor1.setText(_translate("select", "Informática"))
        self.enfer1.setText(_translate("select", "Enfermagem"))
        self.label_4.setText(_translate("select", "2º ano"))
        self.enfer2.setText(_translate("select", "Enfermagem"))
        self.enfer3.setText(_translate("select", "Enfermagem"))
        self.evt3.setText(_translate("select", "Eventos"))
        self.infor3.setText(_translate("select", "Informática"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    select = QtWidgets.QWidget()
    ui = Ui_select()
    ui.setupUi(select)
    select.show()
    sys.exit(app.exec_())

