# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'select2.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
"""
Feito em 2019
@descripition: Classe visual selecionar
@Author:Emily,Clara,Bia
"""

from PyQt5 import QtCore, QtGui, QtWidgets
from Visual.Tinfor1_ui import Ui_Tinfor1
from Visual.Tinfor2_ui import Ui_Tinfor2
from Visual.Tinfor3_ui import Ui_Tinfor3
from Visual.Tguia1_ui import Ui_Tguia1
from Visual.Tguia2_ui import Ui_Tguia2
from Visual.Tevet3_ui import Ui_Tevet3
from Visual.Tenfer1_ui import Ui_Tenfer1
from Visual.Tenfer2_ui import Ui_Tenfer2
from Visual.Tenfer3_ui import Ui_Tenfer3

class Ui_sel(object):
    def setupUi(self, sel):
        self.sell = sel
        sel.setObjectName("sel")
        sel.resize(579, 455)
        self.label = QtWidgets.QLabel(sel)
        self.label.setGeometry(QtCore.QRect(0, -10, 581, 211))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap("fi.png"))
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.gridLayoutWidget = QtWidgets.QWidget(sel)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 210, 561, 231))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.gu2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.gu2.setFont(font)
        self.gu2.setObjectName("gu2")
        self.gridLayout.addWidget(self.gu2, 4, 1, 1, 1)
        self.inf2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.inf2.setFont(font)
        self.inf2.setObjectName("inf2")
        self.gridLayout.addWidget(self.inf2, 4, 2, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 5, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.gu1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.gu1.setFont(font)
        self.gu1.setObjectName("gu1")
        self.gridLayout.addWidget(self.gu1, 2, 1, 1, 1)
        self.inf1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.inf1.setFont(font)
        self.inf1.setObjectName("inf1")
        self.gridLayout.addWidget(self.inf1, 2, 2, 1, 1)
        self.en1 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.en1.setFont(font)
        self.en1.setObjectName("en1")
        self.gridLayout.addWidget(self.en1, 2, 0, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)
        self.en2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.en2.setFont(font)
        self.en2.setObjectName("en2")
        self.gridLayout.addWidget(self.en2, 4, 0, 1, 1)
        self.en3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.en3.setFont(font)
        self.en3.setObjectName("en3")
        self.gridLayout.addWidget(self.en3, 6, 0, 1, 1)
        self.ev3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.ev3.setFont(font)
        self.ev3.setObjectName("ev3")
        self.gridLayout.addWidget(self.ev3, 6, 1, 1, 1)
        self.inf3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.inf3.setFont(font)
        self.inf3.setObjectName("inf3")
        self.gridLayout.addWidget(self.inf3, 6, 2, 1, 1)

        self.retranslateUi(sel)
        self.inf1.clicked.connect(self.botaoTinfor1)
        self.inf2.clicked.connect(self.botaoTinfor2)
        self.inf3.clicked.connect(self.botaoTinfor3)
        self.gu1.clicked.connect(self.botaoTguia1)
        self.gu2.clicked.connect(self.botaoTguia2)
        self.ev3.clicked.connect(self.botaoTevet3)
        self.en1.clicked.connect(self.botaoTenfer1)
        self.en2.clicked.connect(self.botaoTenfer2)
        self.en3.clicked.connect(self.botaoTenfer3)
        QtCore.QMetaObject.connectSlotsByName(sel)

    def retranslateUi(self, sel):
        _translate = QtCore.QCoreApplication.translate
        sel.setWindowTitle(_translate("sel", "Form"))
        self.gu2.setText(_translate("sel", "Guia de Turismo"))
        self.inf2.setText(_translate("sel", "Informática"))
        self.label_5.setText(_translate("sel", "3º ano"))
        self.label_2.setText(_translate("sel", "Selecione a Turma:"))
        self.label_3.setText(_translate("sel", "1º ano"))
        self.gu1.setText(_translate("sel", "Guia de Turismo"))
        self.inf1.setText(_translate("sel", "Informática"))
        self.en1.setText(_translate("sel", "Enfermagem"))
        self.label_4.setText(_translate("sel", "2º ano"))
        self.en2.setText(_translate("sel", "Enfermagem"))
        self.en3.setText(_translate("sel", "Enfermagem"))
        self.ev3.setText(_translate("sel", "Eventos"))
        self.inf3.setText(_translate("sel", "Informática"))

    def botaoTinfor1(self):
        self.Tinfor1 = QtWidgets.QMainWindow()
        self.ui = Ui_Tinfor1()
        self.ui.setupUi(self.Tinfor1)
        self.Tinfor1.show()
        self.sell.close()

    def botaoTinfor2(self):
        self.Tinfor2 = QtWidgets.QMainWindow()
        self.ui = Ui_Tinfor2()
        self.ui.setupUi(self.Tinfor2)
        self.Tinfor2.show()
        self.sell.close()

    def botaoTinfor3(self):
        self.Tinfor3 = QtWidgets.QMainWindow()
        self.ui = Ui_Tinfor3()
        self.ui.setupUi(self.Tinfor3)
        self.Tinfor3.show()
        self.sell.close()

    def botaoTguia1(self):
        self.Tguia1 = QtWidgets.QMainWindow()
        self.ui = Ui_Tguia1()
        self.ui.setupUi(self.Tguia1)
        self.Tguia1.show()
        self.sell.close()

    def botaoTguia2(self):
        self.Tguia2 = QtWidgets.QMainWindow()
        self.ui = Ui_Tguia2()
        self.ui.setupUi(self.Tguia2)
        self.Tguia2.show()
        self.sell.close()

    def botaoTevet3(self):
        self.Tevet3 = QtWidgets.QMainWindow()
        self.ui = Ui_Tevet3()
        self.ui.setupUi(self.Tevet3)
        self.Tevet3.show()
        self.sell.close()

    def botaoTenfer1(self):
        self.Tenfer1 = QtWidgets.QMainWindow()
        self.ui = Ui_Tenfer1()
        self.ui.setupUi(self.Tenfer1)
        self.Tenfer1.show()
        self.sell.close()

    def botaoTenfer2(self):
        self.Tenfer2 = QtWidgets.QMainWindow()
        self.ui = Ui_Tenfer2()
        self.ui.setupUi(self.Tenfer2)
        self.Tenfer2.show()
        self.sell.close()

    def botaoTenfer3(self):
        self.Tenfer3 = QtWidgets.QMainWindow()
        self.ui = Ui_Tenfer3()
        self.ui.setupUi(self.Tenfer3)
        self.Tenfer3.show()
        self.sell.close()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    sel = QtWidgets.QWidget()
    ui = Ui_sel()
    ui.setupUi(sel)
    sel.show()
    sys.exit(app.exec_())

