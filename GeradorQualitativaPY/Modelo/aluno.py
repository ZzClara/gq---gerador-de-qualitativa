"""
@description: Classe modelo onde tera metodos para a sua clase visual
@author:Emily,Clara,Maria Beatriz
"""
class Aluno:
    __mat = ""
    __curso = ""
    __nomeA = ""
    __ano = ""


    def getMat(self):
        return self.__mat
    def setMat(self,m):
        self.__mat = m
    def getCurso(self):
        return self.__curso
    def setCurso(self,c):
        self.__curso = c
    def getNomeA(self):
        return self.__nomeA
    def setNomeA(self,a):
        self.__nomeA = a
    def getAno(self):
        return self.__ano
    def setAno(self,an):
        self.__ano = an