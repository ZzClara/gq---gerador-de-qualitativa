DROP SCHEMA IF EXISTS bd;
CREATE SCHEMA IF NOT EXISTS bd;


CREATE TABLE bd.Usuarios(
	id INT(3) NOT NULL UNIQUE AUTO_INCREMENT,
	nomeU VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	senha VARCHAR(160) NOT NULL,
	PRIMARY KEY(nomeU)
);

CREATE TABLE bd.aluno(
	mat INT(7) NOT NULL UNIQUE,
	nomeA VARCHAR(255) NOT NULL,
	curso VARCHAR(255) NOT NULL,
	ano INT(1) NOT NULL,
	PRIMARY KEY(nomeA)
);

CREATE TABLE  bd.e1(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.e2(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.e3(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.i1(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.i2(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.i3(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.g1(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.g2(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);
CREATE TABLE  bd.ev3(
	id INT NOT NULL AUTO_INCREMENT,
	nomeA VARCHAR(255) NOT NULL,
	nota FLOAT(3) NOT NULL,
	FOREIGN KEY(nomeA) REFERENCES aluno(nomeA)

);