"""
@description: Classe modelo onde tera metodos para a sua clase visual
@author:Emily,Clara,Maria Beatriz
"""
from Controle.Conect import Conexao
from Modelo.gModel import GModel
import mysql.connector
class GControl:
	def inserir(self,modelo):
		# Fazer todas essas instruções usando tratamento de excessões
		try:
			con = Conexao()
			cursor = con.getCon().cursor()
			sql = "INSERT INTO e1(nota,nomeA) VALUES(%s,%s)"
			valores = (modelo.getNota(),modelo.getNomeA())
			cursor.execute(sql,valores)
			con.getCon().commit()
			
			# abrir a conexão e estabelecer cursor
			# criar a sql pra ser executada
			# modificar com as informações usando o encapsulamento da modelo
			# executar a instrução
			# retornar verdadeiro caso ocorra tudo bem

		except mysql.connector.Error as e:
			print("Erro ao conectar ao banco:",e)

		except Exception as e:
			print("Erro geral:",e)