"""
@description: Classe modelo onde tera metodos para a sua clase visual
@author:Emily,Clara,Maria Beatriz
"""
#Criação da classe controle
class Gerar:
	#Método de condição 
	def campo(self,nome):
		if nome != "":
			return True
		else:
			return False
	#Métodos de calcular
	def calcular (self, nota1,nota2,nota3,nota4,nota5,cmb1,cmb2,cmb3,cmb4,cmb5,med):
		if(cmb1=="Sempre"):
			nota1=2
		elif(cmb1=="Quase Sempre"):
			nota1=1.5
		elif(cmb1=="Às Vezes"):
			nota1=1
		elif(cmb1=="Raramente"):
			nota1=0.5
		elif(cmb1=="Nunca"):
			nota1=0

		if(cmb2=="Sempre"):
			nota2=2
		elif(cmb2=="Quase Sempre"):
			nota2=1.5
		elif(cmb2=="Às Vezes"):
			nota2=1
		elif(cmb2=="Raramente"):
			nota2=0.5
		elif(cmb2=="Nunca"):
			nota2=0

		if(cmb3=="Sempre"):
			nota3=2
		elif(cmb3=="Quase Sempre"):
			nota3=1.5
		elif(cmb3=="Às Vezes"):
			nota3=1
		elif(cmb3=="Raramente"):
			nota3=0.5
		elif(cmb3=="Nunca"):
			nota3=0

		if(cmb4=="Sempre"):
			nota4=2
		elif(cmb4=="Quase Sempre"):
			nota4=1.5
		elif(cmb4=="Às Vezes"):
			nota4=1
		elif(cmb4=="Raramente"):
			nota4=0.5
		elif(cmb4=="Nunca"):
			nota4=0

		if(cmb5=="Sempre"):
			nota5=2
		elif(cmb5=="Quase Sempre"):
			nota5=1.5
		elif(cmb5=="Às Vezes"):
			nota5=1
		elif(cmb5=="Raramente"):
			nota5=0.5
		elif(cmb5=="Nunca"):
			nota5=0

		med = (nota1)+(nota2*1)+(nota3*1)+(nota4*1)+(nota5*1)
		
		return str(med)	


	