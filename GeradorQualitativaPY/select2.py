"""
Feito em 2019
@descripition: classe controle de Select
@Author:Emily,Clara,Bia
"""
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.select2_ui import Ui_sel

import sys
app = QtWidgets.QApplication(sys.argv)
sel = QtWidgets.QWidget()
ui = Ui_sel()
ui.setupUi(sel)
sel.show()
sys.exit(app.exec_())