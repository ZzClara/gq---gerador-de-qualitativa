"""
Feito em 2019
@description: Tela do executavel da tela instruções
@author:Emily,Clara,Bia
"""
#Importaçao e executavel da tela Instruções
from PyQt5.QtWidgets import QApplication, QMainWindow
from Visual.instrucoes_ui import Ui_janelai
import sys
app = QApplication(sys.argv)
ins = QMainWindow()
janela2 = Ui_janelai()
janela2.setupUi(ins)
ins.show()
sys.exit(app.exec_())  